# code-communists

Previously there were some [other exposés](https://gitlab.com/tcrowe/nodejs-communist-problem) about Communists and Fascists in the software industry. The reason I've chosen the term "Communists" is because their ideology is International Socialism, not National Socialism, so they are not technically Fascists. It's a semantic difference.

Clone or mirror this repo in order to enforce your right, and all our right, to free speech. PRs and issues are welcome to build our knowledge base! Please give a ⭐️ if you liked it.

Thank you for your consideration of this important issue negatively affecting our industry.

## What is a Code Communist?

Check those that apply to you or someone you know.

1. [ ] People or groups that enthusiastically or unwittingly conform to Communist ideology by:
2. [ ] Excluding straight white males where possible and neglecting and providing hostility for those they are around
3. [ ] Giving preferential treatment for homosexual white males and non-gender-conforming white individuals
4. [ ] Reacts in a hostile way to perceived slights about other races than their own
5. [ ] Believes in establishing a brown and black underclass by paying a fraction of the price for their labor
6. [ ] Denies race or gender exist while trying to provide as much benefit as possible to other races and genders than their own
7. [ ] Is a [Bugman](https://www.urbandictionary.com/define.php?term=Bugman)
8. [ ] Censors views they don't understand or agree with so nobody can see it
9. [ ] Generally has a gynocentric, matriarchal, and effeminate personality
10. [ ] Is blind to their self

If you check off any of the boxes above it's unlikely to be true because the number ten excludes anyone who can see their true self. But, if you're one of the few exceptions who knows and accepts themselves then more likely you've ascended to the [Inner Party](https://infogalactic.com/info/Inner_Party). 👌🏻

## The Code Communist Strategy

![1984_Social_Classes_alt.svg](./img/1984_Social_Classes_alt.svg)

[Download "1984" social classes](https://commons.wikimedia.org/wiki/File:1984_Social_Classes_alt.svg)

The government has taught them from Pre-Kindergarten to at least High School. (Pre-K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12) That is at least thirteen years being in the custody of the state. More if they attend a college. They watch TV or Netflix propaganda all night.

They simply don't know anything except what the the government has told them. Beyond that there is cognitive dissonance if they try to think critically about the information. They were never taught to think for themselves and it would hurt their brain to try.

Imagine that these people are stuck, permanently, in a life-long government institution with a culture of tattle-tale school girls. The way to rise in rank is brown-nosing the person above you in the social class, just the same as they did with the teacher, for all those years. They believe if they attack enemies of the master they will have a higher social status and reward. In reality it's [Beggar thy neighbor](https://infogalactic.com/info/Beggar_thy_neighbour) policy at all levels of society.

Psychologically, if a person is mentally abused this way, as most people are, they become frustrated, confused, and self-hating. They seek to be destructive and their ideology, given to them by the masters, is a convenient outlet for their destructive feelings.

If we put aside their evil actions can you have pity on them for the childhood they went through? Now can we begin to deal with the problem and allow people to have a mature adulthood with some dignity?

👨‍🎓 [Educator John Taylor Gatto on our Prussian Education](https://www.youtube.com/watch?v=YQiW_l848t8&list=PL463AA90FD04EC7A2) 👩‍🎓

## Table of Contents

+ [Rod Vagg removed from node](#rod-vagg-removed-from-node)
+ [NPM CEO Isaac Schlueter calls for fewer white men in tech](#npm-ceo-isaac-schlueter-calls-for-fewer-white-men-in-tech)
+ [node.js Communists Confronted by Tony Crowe, JS Developer](#nodejs-communists-confronted-by-tony-crowe-js-developer)
+ [GitHub cooperates with totalitarian dictators](#github-cooperates-with-totalitarian-dictators)
+ [Microsoft-GitHub Bans people being killed by U.S. Oligarchs so they cannot speak 2019](#microsoft-github-bans-people-being-killed-by-us-oligarchs-so-they-cannot-speak-2019)
+ [Slack Bans Iranians for U.S. Oligarchs](#slack-bans-iranians-for-us-oligarchs)
+ [Linus Torvalds and The Linux Foundation](#linus-torvalds-and-the-linux-foundation)
+ [James Damore Outs Google as Ideological Echochamber](#james-damore-outs-google-as-ideological-echochamber)
+ [Project Veritas, Zach Voorhees, Google Machine Learning Fairness](#project-veritas-zach-voorhees-google-machine-learning-fairness)
+ [Machine Learning Fairness](#machine-learning-fairness)
+ [Popular culture](#popular-culture)
  + [Bill Maher commedian "White Shame" skit](#bill-maher-commedian-white-shame-skit)
  + [#FellowWhitePeople twitter meme](#fellowwhitepeople-twitter-meme)
+ [Projects](#projects)
+ [Activists](#activists)
+ [Caveats](#caveats)
+ [Common rebuttals](#common-rebuttals)
  + [Liberal is not Communist](#liberal-is-not-communist)
  + [Socialist is not Communist](#socialist-is-not-communist)
  + [Progressive is not Communist](#progressive-is-not-communist)
  + [I'm not a Communist](#im-not-a-communist)
  + [That's your opinion](#thats-your-opinion)
  + [The views of some don't represent all](#the-views-of-some-dont-represent-all)
+ [Copying, license, and contributing](#copying-license-and-contributing)

## Rod Vagg removed from node

[Requesting Rod to step down or be removed from the CTC](https://github.com/nodejs/CTC/issues/165) [html](./backups/requesting-rod-to-step-down-or-be-removed-from-the-ctc-issue-165-nodejs-ctc.html) [png](./backups/requesting-rod-to-step-down-or-be-removed-from-the-ctc-issue-165-nodejs-ctc.png)

The coalition was spearheaded by [@jasnell](https://github.com/jasnell).

![jasnell profile minor protest](./projects/node/jasnell-profile-minor-protest.png)

> One who writes code. #notmypresident
> \-jasnell profile

For those outside the U.S.A. jasnell is signaling rejection to the democratically elected President of U.S.A, Donald Trump. [More on his twitter](https://twitter.com/jasnell) and in [nodejs-communist-problem](/tcrowe/nodejs-communist-problem). His organization NearForm is attracting people with these kinds of sentiments.

["Why I’m leaving the Node.js project" by Brian Hughes, Microsoft](https://medium.com/@nebrius/why-im-leaving-the-node-js-project-bff946845a77) [html](medium.com-nebrius_why-im-leaving-the-node-js-project-bff946845a77.html) [png](medium.com-nebrius_why-im-leaving-the-node-js-project-bff946845a77.png)

Hughes claims to have been demoralized, exhausted, and quitting, yet continues to this day persecuting people out of node who don't share his peculiarities. It appears to have been a ruse.

!["Diverse" Brian Hughes of Microsoft "Evangelist"](./projects/node/diverse-brian-hughes-microsoft.png)

## NPM CEO Isaac Schlueter calls for fewer white men in tech

> Schlueter, who is a white man in tech, submitted his own speaking proposal to the conference, exempted himself from his virtue signal to state: “If the speaker proposals are all/mostly from white dudes, the quality of content suffers due to a limited pool. This is basic math.”

["Tech CEO Isaac Schlueter Calls For Fewer White Men In Tech" by Ian Miles Cheong, Daily Caller](https://dailycaller.com/2017/09/25/tech-ceo-isaac-schlueter-calls-for-fewer-white-men-in-tech/)

**Does Isaac Schlueter self-identify as white though?** For those with a keen political sense they may understand the subtleties of Mr. Schlueter's motivations. Is it a case of [#FellowWhitePeople](#fellowwhitepeople-twitter-meme)?

[The tweets are deleted.](https://twitter.com/search?l=&q=%22white%22%20from%3Aisaacs&src=typd)

## node.js Communists Confronted by Tony Crowe, JS Developer

Tony Crowe, originator of this repo, confronted the nodejs team with a sincere warning about the negative effects of keeping their exclusive Code Communist community going that way. He provided ideas to identify their ideological bias. Predictably it was immediately censored from their GitHub issues.

[tcrowe/nodejs-communist-problem](/tcrowe/nodejs-communist-problem) A list of evidence and group discussion about Totalitarian censorship by nodejs Code Communists.

## GitHub cooperates with totalitarian dictators

[Censorship of GitHub](https://infogalactic.com/info/Censorship_of_Github)

There was evidence of government corruption on GitHub. The users put their content on GitHub knowing it would be difficult for their government to censor. Their governmemnt did succeed in censoring DNS, MITM, and other techniques. It effectively blocked many of their nation's developers from doing their work. China, Turkey, Russia, India, the so-called B.R.I.C.S. faction, put pressure on GitHub to remove it, and GitHub cooperated so they are complicit in government crimes against their citizens.

## Microsoft-GitHub Bans people being killed by U.S. Oligarchs so they cannot speak 2019

Nations affected

+ Iran
+ Syria
+ Crimea
+ Cuba
+ North Korea

You're allowed to use GitHub unless the place you live is being bombed on behalf of U.S. Oligarchs.

> GitHub used to be an open and free platform for everyone, but it has decided to restrict Iranian accounts from contributing and being part of the open-source ecosystem. Although we understand GitHub might make this decision under the pressure of US government, we were expecting a more respectful action from GitHub.

<https://github.com/1995parham/github-do-not-ban-us>

> Acknowledging the actions on Twitter, GitHub CEO Nat Friedman said that the company “unfortunately had to implement new restrictions on private repos and paid accounts in Iran, Syria, and Crimea.”

> It is painful for me to hear how trade restrictions have hurt people. We have gone to great lengths to do no more than what is required by the law, but of course people are still affected. GitHub is subject to US trade law, just like any company that does business in the US.

> To comply with US sanctions, we unfortunately had to implement new restrictions on private repos and paid accounts in Iran, Syria, and Crimea.

<https://twitter.com/natfriedman/status/1155311121038864384?ref_src=twsrc%5Etfw>

⚠️ This is the classic [Neuremberg Defense](https://infogalactic.com/info/Superior_orders) used by Hitler's soldiers after the war.

## Slack Bans Iranians for U.S. Oligarchs

<https://slackhq.com/an-apology-and-an-update>

You're allowed to chat unless you were born in Iran. U.S.-based oligarchs in the government tell Slack to ban so they ban. Look how far their ostensible Communist support for non-white people goes. (P.o.C. is the derogatory term Communists use to lump all people without light skin into one blob of people.)

## Linus Torvalds and The Linux Foundation

They had a big row over Linus' rude language and used that as a reason to ram The Code of Conduct down the throat of the Linux community.

Tsk, tsk, Linus. Your cussin' angered the Code Communists! 😠

<https://github.com/torvalds>

[The Leftists Behind Ousting Of Linus Torvalds, New CoC, & War Being Waged For Linux Partial Rant](https://www.youtube.com/watch?v=VVSctD-y-pc)

[Linux Vs SJW's In The Battle For The Internet](https://www.youtube.com/watch?v=D23mYwPkdyw) talks about the license which allows some developers to pull out their code in response to banning, Code of Conduct, and Covenant issues. Source article [Linux developers threaten to pull "kill switch"](https://lulz.com/linux-devs-threaten-killswitch-coc-controversy-1252/).

The news "Linus takes a break from Linux" came out and others reported that Linus "stepped down" from Linux. More information is needed to know the true history but he's not out at this time. *sources needed*

Linus Torvalds explained in a video that he did not want to be viewed as a "White Supremacist" so he capitulated to the pressure. *video needed*

## James Damore Outs Google as Ideological Echochamber

a.k.a. The Google Memo

+ <https://twitter.com/JamesADamore>
+ <https://infogalactic.com/info/Google%27s_Ideological_Echo_Chamber>
+ [Google Memo: Fired Employee Speaks Out! | James Damore and Stefan Molyneux](https://www.youtube.com/watch?v=TN1vEfqHGro)
+ <https://firedfortruth.com>

The courage of the young man is amazing. To tell the truth about such a large and powerful organization is no small feat. James Damore published and spoke publicly about the ideology and discrimination in Google organization.

## Project Veritas, Zach Voorhees, Google Machine Learning Fairness

+ <https://www.projectveritas.com/2019/08/14/google-machine-learning-fairness-whistleblower-goes-public-says-burden-lifted-off-of-my-soul/>
+ <https://www.youtube.com/watch?v=g1VeElBAeas>

This person, Zach Voorhees, through Project Veritas, an undercover investigation organization released information related to something called "Machine Learning Fairness" or "ML Fairness." In essence the story details some technical ways that Google reduces visibility of information they disagree with and substitute information which is more in line with their political opinions. The data dump details some of their **discriminatory hiring practices** which go unpunished by the government.

For long people were searching for images "white family" or "happy family" and there would not be any white family in the results. Instead it would provide only non-white people into the results, at least for the first page. Similarly, for everything from text, to images, to video, and everything the results are systematically reversed from what you might be searching for. It's a bizarre, yet predictable choice, if you know their ideology.

In the data there is a list of search terms which they say are part of a banned list or a list where the results are highly perverted versus what people might be searching for. They include things like well-known mass shooting events where people have not believed the government.

I'm not sure what to make of it but this person might have been in [this video entitled "VIDEO: YouTube senior software engineer: 'I escaped' active shooting situation"](https://www.youtube.com/watch?v=IO9m6wZc-ps).

> A senior software engineer with YouTube named Zach Voorhees spoke to ABC7 News and described what he saw when the shooting broke out Tuesday afternoon. Full story here: <https://abc7ne.ws/2pZzRp2>.

That page returns a 404 now. You can see that even though the end result is a 404 it is still in their database to redirect to this URL:
<https://abc7news.com/video-youtube-senior-software-engineer-i-escaped-active-shooting/3297690/>

There was no backup of it on archive.org. If you can find it please submit an issue or PR.

## Machine Learning Fairness

When they talk about "Fairness" it doesn't mean the same thing as what is in the dictionary. It is a political or New Speak word. Don't let "AI" or "Machine Learning" scare you. It's just a technique for efficiently sorting information.

+ [Microsoft Research: The Emerging Theory of Algorithmic Fairness](https://www.youtube.com/watch?v=g-z84_nRQhw)
+ <https://developers.google.com/machine-learning/fairness-overview/>
+ <https://cloud.google.com/inclusive-ml/> - INCLUSIVE! Very inclusive!
+ <https://www.ibm.com/blogs/research/2018/09/ai-fairness-360/>

> As algorithms reach ever more deeply into our daily lives, increasing concern that they be “fair” has resulted in an explosion of research in the theory and machine learning communities. This talk surveys key results in both areas and traces the arc of the emerging theory of algorithmic fairness.

Note particularly "**explosion of research.**" It could be translated to mean "a lot of money flowing from the government into this idea."

> Human decision makers are susceptible to many forms of prejudice and bias, such as those rooted in gender and racial stereotypes.

<https://www.microsoft.com/en-us/research/blog/machine-learning-for-fair-decisions/>

Note the header image.

It links to this page about [fairlearn](https://github.com/Microsoft/fairlearn). One of the headings is "What we mean by fairness." They are clear to say this is not about "fairness" the way you think of it from the dictionary. It should have been capitalized to say "Fairness."

Contributors sourced from [here](https://raw.githubusercontent.com/fairlearn/fairlearn/master/AUTHORS.md).

+ [Sarah Bird](https://github.com/slbird)
+ [Miro Dudik](https://github.com/MiroDudik)
+ [Richard Edgar](https://github.com/riedgar-ms)
+ [Brandon Horn](https://github.com/rihorn2)
+ [Roman Lutz](https://github.com/romanlutz)
+ [Vanessa Milan](https://www.microsoft.com/en-us/research/people/vmilan/)
+ [Sangeeta Mudnal](https://www.microsoft.com/en-us/research/people/smudnal/)
+ [Mehrnoosh Sameki](https://github.com/mesameki)
+ [Hanna Wallach](https://www.microsoft.com/en-us/research/people/wallach/)
+ [Vincent Xu](https://github.com/vingu)
+ [Beth Zeranski](https://github.com/bethz)

> drawing on the deeper context surrounding these issues from sociology

\-[Microsoft's F.A.T.E. Team](https://www.microsoft.com/en-us/research/group/fate/)

For those in the know "issues from sociology" are only ideas from Communism that they teach and learn in universities.

## Popular culture

Just for context we're listing some pop culture and memes about these types of Communists who are becoming infamous.

### Bill Maher commedian "White Shame" skit

[Bill Maher "White Shame" skit](https://www.youtube.com/watch?v=T0q2ZR4nBuE) makes fun of self-hating white Communists. It has over 1.5 million views, which could be also be censored by YouTube Communists, but which is still interesting.

> You think it's hard being a black man in a white man's world? Try being a white woman who feels bad about you being a black man in the white man's world!
> \-Bill Maher

Because Code Communists don't understand reporting facts, Bill Maher said that. We didn't say that. We use this minus `-Bill Maher` notation and the blockquote to communicate he said it. He is allowed to tell you that because he self-identifies as a straight Jewish male. Do you understand?

### #FellowWhitePeople twitter meme

People pretending to be a self-hating white but they are some other race. We don't claim here to know the motivation for this.

## Projects

Document run-ins with Code Communists and create a record and warn others.

+ [Automattic](./projects/automattic/index.md) - WordPress publishing
+ [eslint](./projects/eslint/index.md) - JS code quality checker/linter
+ [express](./projects/express/index.md) - HTTP library
+ [F-Droid](./projects/f-droid/index.md) - Android apps app
+ [Gitea](./projects/gitea/index.md) - git server
+ [GitHub](./projects/github/index.md) - git server
+ [GitLab](./projects/gitlab/index.md) - git server
+ [Hexo](./projects/hexo/index.md) - static site generator
+ [Mailchimp](./projects/mailchimp/index.md) - email and marketing
+ [Microsoft](./projects/microsoft/index.md) - software giant
+ [NavCoin](./projects/navcoin/index.md) - cryptocurrency
+ [Netlify](./projects/netlify/index.md) - static site host
+ [Node.js](./projects/node/index.md) - JavaScript tools
+ [npm](./projects/npm/index.md) - node package manager
+ [PayPal](./projects/paypal/index.md) - payment service
+ [reddit](./projects/reddit/index.md) - social media
+ [TravisCI](./projects/travis-ci/index.md) - CI/CD
+ [vue](./projects/vue/index.md) - HTML, CSS, and JS GUI library
+ [YouTube](./projects/youtube/index.md)

## Activists

These individuals have gone on the offensive to make their contributions to Communism.

+ [Caleb Rogers](./activists/caleb-rogers/index.md)

## Caveats

None of this information should be considered an exhaustive list of the phenomenon. There are many more peculiar behaviors that go along with it. Many more cases go undiscovered.

Do you know some that we don't? Create a PR or issue and we'll add it in.

Do you disagree? File with us your disagreement or protestations and we will consider the reasoning and evidence.

## Common rebuttals

### Liberal is not Communist

Liberals have the same thoughts, views, opinions, and policies as Communists. For example having the government take away money from productive people and give it to unproductive people. Another example is reverence for GULAG, FEMA, slave labor "Re-Education" camps. There are two different words but it is the same ideology.

### Socialist is not Communist

Socialists have the same thoughts, views, opinions, and policies as Communists. Some say Socialism started as a sincere grassroots ideology. At some point it was coopted by the rulers and it is the dominant ideology of all governments. There are two different words but it is the same ideology.

[Anabaptist Christians and White Western European men invented Socialism.](https://infogalactic.com/info/History_of_socialism) Modern Socialists and Communists base their rhetoric on the writings of Karl Marx.

Karl Marx ✡️ the self-hating Jewish antisemite attached to the movement with industrial magnate and financier Engels. Marx publically advocated for "The Working Class" and he had an illegitimate son by raping his live-in maid. Eventually [he threw her out on the street and refused to acknowledge his son.](https://www.opindia.com/2019/06/karl-marx-had-impregnated-his-maid-and-didnt-own-up-the-child/)

[📼 The Truth About Karl Marx](https://www.youtube.com/watch?v=yA2lCBJu2Gg)

Hitler was a **National Socialist**

> The National Socialist German Workers' Party (German: About this sound Nationalsozialistische Deutsche Arbeiterpartei (help·info), abbreviated NSDAP), commonly referred to in English as the Nazi Party (English /ˈnɒtsi, ˈnætsi/),[6] was a far-left[7] political party in Germany that was active between 1920 and 1945, that created and supported the ideology of National Socialism. Its precursor, the German Workers' Party (Deutsche Arbeiterpartei; DAP), existed from 1919 to 1920.

[Read more about Nazi ideology a.k.a. National Socialism](https://infogalactic.com/info/Nazi_Party)

> Nazi political strategy focused on anti-big business, anti-bourgeois, and anti-capitalist rhetoric, although this was later downplayed in favor of anti-Semitic and anti-Marxist themes.[10]

[Joseph Stalin](https://infogalactic.com/info/Joseph_Stalin) the dictator presided over **Union of Soviet Socialist Republics** or U.S.S.R. and the GULAG slave labor camps.

> Joseph Stalin (birth surname: Jughashvili; 18 December 1878[1] – 5 March 1953) was the leader of the Soviet Union from the mid-1920s until his death in 1953. Holding the post of the General Secretary of the Central Committee of the Communist Party of the Soviet Union, he was effectively the dictator of the state.

[President Xi Jinping of C.C.P., The Chinese Communist Party](https://infogalactic.com/info/Xi_Jinping), presides over GULAG slave labor camps in China.

> Xi believes that China should be 'following its own path' and that a strong authoritarian government is an integral part of the "China model", operating on a "core socialist value system", which has been interpreted as China's alternative to Western values.

### Progressive is not Communist

Progressives have the same thoughts, views, opinions, and policies as Communists. For example having the government take away money from productive people and give it to unproductive people. Another example is creating a black and brown underclass similar to Communist GULAG slave labor camps but with better marketing. They pay them less than their White European counterparts and provide incentives to emigrate to The West. There are two different words but it is the same ideology.

### I'm not a Communist

Every person raised in the modern society goes through Communist public school. They call it Education and the government institutions are constructed similarly to prisons and slave labor camps. Many include barbed wire, LGBT clubs, and racial violence. The teachers are unionized government employees, mostly Democrat Party supporters, and Communists.

Students matriculate to so-called Higher Education. Universities are government-funded against the will of the people and wasted on teaching Communist ideology to students. Those students graduate with student loan debt and a few go on to become party leaders in the government. Students and professors make up excuses for failures of government and [justify mass casualties via war.](https://www.youtube.com/watch?v=R0WDCYcUJ4o)

You just need to look at the words and actions. Even if someone doesn't self-identify as a Communist but their thoughts and ideas align with it they are a Communist implicitly. If they stay silent or lend support to Communists they are a Communist.

Without an effort of will, determination, and knowledge counter to the system why would you be other than what the rulers designed for you?

See also [The Code Communist Strategy](#the-code-communist-strategy) for more about hierarchy, history, and psychological factors.

### That's your opinion

opinion - n

> a view or judgment formed about something, not necessarily based on fact or knowledge

Facts and evidence are distinct from opinions. Opinions are *unverified* thoughts and ideas. Facts and evidence are *empirically verified*, meaning they reflect real life events and objects.

The reason why people want to cast your idea as mere opinion is that they wish to distance themselves emotionally from it. In their mind, if they can perceive the stimuli, your evidence, as an opinion then they can more easily dismiss it. This is because they know and fear the truth and consequences if they accept the evidence as it is.

### The views of some don't represent all

It's commonly said that a few bad apples don't represent everyone in the community. If that were so there would be others to counter them and coach them away from their destructive ideology. If Communism were not representative of their group there would be evidence and more than one or two dissenters.

A few dissenters do not invalidate a generalization although it can invalidate a Scientific thesis.

## Copying, license, and contributing

Copyright (C) Tony Crowe 2020 <https://tonycrowe.com/contact/>

Thank you for using and contributing to make code-communists better.

⚠️ Please run `npm run prd` before submitting a patch.

⚖️ code-communists is free and unlicensed. Anyone can use it, fork, or modify and we the community will try to help whenever possible.
