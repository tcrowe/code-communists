# f-droid

Android apps app

+ <https://f-droid.org>
+ <https://gitlab.com/fdroid>

## Table of Contents

+ [Censorship of Gab](#censorship-of-gab)

## Censorship of Gab

+ <https://news.ycombinator.com/item?id=20466730>
+ <https://f-droid.org/en/2019/07/16/statement.html>

Gab, the social network, was the target of a smear and censorship campaign. Whoever runs F-Droid fell for the trick and banned Gab or something to do with the Fediverse. In the process they exposed F-Droid as a hard-left organization.
