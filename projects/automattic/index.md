# Automattic

<https://automattic.com/>

"DIVERSITY" link right on the front page

> In 2014 we started to work, as a company, on facilitating spaces for discussions about diversity at Automattic

Customers and employees of Automattic pay extra for:

## Table of Contents

+ [Diversity and Inclusion](#diversity-and-inclusion)
+ [Diversity and Inclusion Partners](#diversity-and-inclusion-partners)
  + [AlterConf](#alterconf)
  + [Mother Coders](#mother-coders)
  + [TechWomen](#techwomen)
  + [The Representation Project](#the-representation-project)
  + [Outreachy](#outreachy)

## Diversity and Inclusion

<https://automattic.com/diversity-and-inclusion/>

> strive to increase the visibility of traditionally underrepresented groups.

What is the right amount amount of Representation?

<https://automattic.com/work-with-us/>

> In 2014 we started to work, as a company, on facilitating spaces for discussions about diversity at Automattic

> Educate yourself about diversity

> Whether it’s subscribing to the Geek Feminism Wiki, checking out the The Huffington Post’s Workplace Diversity articles,

> Listen to the NPR

> Find out more about obstacles and solutions for underrepresented minorities in technology.

When will there be enough Representation?

> We hold various religious beliefs — or none.

Or none

> We encourage visiting our private online spaces that include places: For those who identify as LGBTQ+. For those who identify as women and wish to support and connect with each other.

> Did you know there are places in the world where being homosexual is considered a sin, or even a crime?

## Diversity and Inclusion Partners

> In the past we’ve supported organizations like Mother Coders, TechWomen, The Representation Project, and Outreachy. We’re especially proud of supporting Accelerate.lgbt.

> We’re thrilled to work and partner with other companies and institutions that share our values.

Maybe not too enthusiastic about who's already working there?

### AlterConf

<https://www.alterconf.com/>

> Thanks For Helping Us Change The Face Of Tech & Gaming!

Changing the "face"

### Mother Coders

### TechWomen

### The Representation Project

### Outreachy
