# TravisCI

<http://travisci.com/>

> Berlin, Germany

TravisCI customers and employees are paying for:

## Table of Contents

+ [Diversity and Inclusion Partners](#diversity-and-inclusion-partners)
  + [AlterConf](#alterconf)
  + [Rails Girls](#rails-girls)
  + [Aboriginal Legal Service](#aboriginal-legal-service)
  + [Caritas supporting refugees in Berlin](#caritas-supporting-refugees-in-berlin)
  + [LSVD](#lsvd)
  + [Moabit Hilft](#moabit-hilft)
  + [Planned Parenthood](#planned-parenthood)
  + [Sea Watch](#sea-watch)
  + [Travis Foundation](#travis-foundation)

## Diversity and Inclusion Partners

<https://blog.travis-ci.com/2016-02-02-travis-cis-charitable-giving-2015>

> Beyond our charitable giving, we’ve also continued to sponsor diversity tickets for several conferences

Is the idea that they are seeking out people who are not white and not male to give tickets to? 🤔

### AlterConf

<https://www.alterconf.com/>

> Thanks For Helping Us Change The Face Of Tech & Gaming!

### Rails Girls

<https://blog.travis-ci.com/open-call-rails-girls-summer-of-code>

> How we work to improve diversity in tech with Rails Girls Summer of Code

### Aboriginal Legal Service

<http://www.alsnswact.org.au/>

> We are proudly Aboriginal

> End strip searches of Aboriginal kids

😬 Dang... yeah please end that.

### Caritas supporting refugees in Berlin

<https://www.betterplace.org/en/projects/33291-hilfe-fur-fluchtlinge-in-berlin-und-brandenburg>

More non-German people in Germany

### LSVD

<https://berlin.lsvd.de/der-lsvd/unser-programm/>

Translated from Detusch

> All human beings are born free and equal in dignity and rights. This great promise of the Universal Declaration of Human Rights must also be fulfilled for lesbians and gays, as well as for bisexuals, transgender, trans and intersex people. The Lesbian and Gay Association in Germany (LSVD) is working for this goal.

### Moabit Hilft

<https://www.moabit-hilft.com/>

Translated from Deutsch

> We are the non-partisan lobby for refugees and stand up for people in Berlin Moabit and beyond.

### Planned Parenthood

<https://www.plannedparenthood.org/>

Abortions

### Sea Watch

<https://sea-watch.org/>

![./sea-watch.png](./sea-watch.png)

### Travis Foundation

<https://foundation.travis-ci.org/>

> support projects that provide opportunities to newcomers, foster diversity

Selecting a photo of all women on the front
