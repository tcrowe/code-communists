# Netlify

A static hosting company.

<https://www.netlify.com/about/>

> We’re ~40% woman or non-binary, and have about half as many nationalities as we are team members!

> our HQ in San Francisco

## Table of Contents

+ [Open Source Policy](#open-source-policy)
+ [Team](#team)

## Open Source Policy

<https://www.netlify.com/legal/open-source-policy/>

Requires that:

> Features a Code of Conduct at the top level directory of the project repository or prominently in the documentation (with a link in the navigation, footer, or homepage).

Last years they offered to host [hexo](../hexo) and now the hexo people [are complying](https://github.com/hexojs/hexo/pull/3962) with the new rule.

A publicly Liberal company is requiring Code of Conduct...

But [SukkaW says](../hexo) that's a conspiracy theory. 😦

> IMHO, there shall be no place for neither alt-right conspiracy nor alt-left conspiracy in open source community. Try seeking professional help instead of trolling.

## Team

![netlify-perry-eising01.png](./netlify-perry-eising01.png)

![netlify-perry-eising02.png](./netlify-perry-eising02.png)

* * *

![netlify-jessica-parsons01.png](./netlify-jessica-parsons01.png)

* * *

![netlify-leslie-wein01.png](./netlify-leslie-wein01.png)

![netlify-leslie-wein02.png](./netlify-leslie-wein02.png)

![netlify-leslie-wein03.png](./netlify-leslie-wein03.png)

* * *

![netlify-sympathy-hire.png](./netlify-sympathy-hire.png)

![netlify-hire-women.png](./netlify-hire-women.png)

* * *

![netlify-divya-sasidharan01.png](./netlify-divya-sasidharan01.png)

![netlify-divya-sasidharan04.png](./netlify-divya-sasidharan04.png)

![netlify-divya-sasidharan02.png](./netlify-divya-sasidharan02.png)

![netlify-divya-sasidharan03.png](./netlify-divya-sasidharan03.png)

* * *

![netlify-ingrid-epure01.png](./netlify-ingrid-epure01.png)

![netlify-ingrid-epure02.png](./netlify-ingrid-epure02.png)

![netlify-ingrid-epure03.png](./netlify-ingrid-epure03.png)

* * *

![netlify-david-calavera01.png](./netlify-david-calavera01.png)

![netlify-david-calavera03.png](./netlify-david-calavera03.png)

![netlify-david-calavera02.png](./netlify-david-calavera02.png)

![netlify-david-calavera04.png](./netlify-david-calavera04.png)
