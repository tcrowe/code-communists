# Hexo

<https://hexo.io>
<https://github.com/hexojs>

Hexo is a static website app that has flexible options for authoring and templating. It's in the [top 10-20 of static site generators](https://www.staticgen.com/).

## Table of Contents

+ [Code of Conduct](#code-of-conduct)
  + [Commentary from tcrowe](#commentary-from-tcrowe)
+ [SukkaW accuses tcrowe of misbehavior](#sukkaw-accuses-tcrowe-of-misbehavior)
  + [Commentary from tcrowe about alleged misbehavior](#commentary-from-tcrowe-about-alleged-misbehavior)
+ [Hexo's Multicultural Community](#hexos-multicultural-community)
+ [Compliance to San Francisco Netlify](#compliance-to-san-francisco-netlify)
+ [Minimalization from ertrzyiks](#minimalization-from-ertrzyiks)

## Code of Conduct

2019-12-15: Project founder [tommy351](https://github.com/tommy351), who rarely comments, announced the PR for a Code of Conduct for the organization.

+ <https://github.com/orgs/hexojs/teams/core/discussions/23>
+ <https://github.com/hexojs/hexo/pull/3962>

> Netlify recently updated their policy and they required the repo to have a code of conduct and a license. I chose Contributor Covenant currently because it seems to be a very popular CoC.

\-[tommy351](https://github.com/tommy351)

> @tcrowe I see CoC, in the context of Hexo, as merely a formality for Netlify's policy. I believe my fellow teammates wouldn't abuse CoC to persecute others simply for their political views.

\-[curbengh](https://github.com/curbengh)

This was after a warning about the danger of Codes of Conduct (C.o.C), and the movement behind it.

> I'd caution the hexo org against making hasty decisions on CoC. Please read my repo here how American liberals invented CoC to persecute their political enemies: <https://gitlab.com/tcrowe/nodejs-communist-problem>

\-[tcrowe](https://gitlab.com/tcrowe)

In response to tcrowe's warning, contributor [SukkaW](https://github.com/SukkaW) replied:

> @tcrowe That is why we definitely need a CoC that designed to avoid persecution & hatred, which you just did.

> It is FUNDAMENTALLY WRONG to: 1. speculate that certain community member (no matter from @hexojs or @nodejs) shall have particular political views 2. bring personal political point of view into multicultural community 3. utilize any open source project to promote conspiracy theory.

> IMHO, there shall be no place for neither alt-right conspiracy nor alt-left conspiracy in open source community. Try seeking professional help instead of trolling.

\-[SukkaW](https://github.com/SukkaW)

### Commentary from tcrowe

I'd just like to go over some of the language used in the C.o.C. discussion starting with contributor SukkaW.

![sukkaw-github-profile.png](./sukkaw-github-profile.png)

> It is FUNDAMENTALLY WRONG to: 1. speculate that certain community member (no matter from @hexojs or @nodejs) shall have particular political views

It's not immoral even if that had been done. I doubt this person has any background to know things about morals. In my case I saw and assumed that they were adding this document due to `popularity` or conformity. I believed they were unaware of the political aspects and probably had not read the document and probably had not heard about Linus Torvalds.

> 2. bring personal political point of view into multicultural community

This is interesting that a person can contradict themselves in a sentence fragment. Multiculturalism is a political strategy pursued by some of the ruling factions in the world. Maybe SukkaW is trying to communicate some support for that politics.

> 3. utilize any open source project to promote conspiracy theory.

> IMHO, there shall be no place for neither alt-right conspiracy nor alt-left conspiracy in open source community.

Assertion without evidence or reference to any specific information. (a.k.a. baseless accusation) As far as I know the term "Conspiracy Theory" originated after the J.F.K. assassination when the C.I.A. wanted to publicly smear people who didn't believe the government. It's just a guess that contributor SukkaW wishes to use the same tactic against political enemies.

United States Republican Presidents George Bush Junior and Senior talking about "Conspiracy Theories":

<https://www.youtube.com/watch?v=3O0LB7M36AI>

SukkaW says "IMHO" but I do not see any sign that this person has humility. I see the opinions though.

> Try seeking professional help instead of trolling.

After lying, misrepresenting, and using [really old C.I.A. smears](https://www.zerohedge.com/news/2015-02-23/1967-he-cia-created-phrase-conspiracy-theorists-and-ways-attack-anyone-who-challenge) I'm advised to seek help. I'm not sure if they mean I should see a psychiatrist or psychologist. I would guess psychiatrist because they would want me to be prescribed psychopharmaceuticals given the disgust this person has for me.

The other hexo organization members did not defend me in any way, despite my kindness and contributions in the past. That tells you a lot about their character. Can you live up to a code of conduct if you cannot even defend a contributor in a discussion about C.o.C.?

I linked SukkaW user to a document about [Psychological Projection](https://en.wikipedia.org/wiki/Psychological_projection). They say it's a "defense mechanism" but it's an offensive way people lie about you in things that they, themselves, are doing.

Beyond that the person is concealing who they are online. They contribute to a lot of open source projects and are generous with their time. The discussion has illuminated that they have a corrupt personality and are probably left-leaning politically.

## SukkaW accuses tcrowe of misbehavior

This URL might be private but this was the source. I'll include the content here.
<https://github.com/orgs/hexojs/teams/core/discussions/23>

> During the early discussion about the Code of Conduct, @tcrowe has tried to bring his political idea about the Code of Conduct, saying the Code of Conduct is a tool used to persecute dissidents. From the repo he posted, I found his showing hatred to the contributors from Node.js community:

> This is about evidence of nodejs community members harass and persecute people they disagree with. It is a safe space to talk about the nodejs community slip into the totalitarian ideology called Communism. Evidence is accepted via PRs but opinions are limited to issues. We welcome feedback from all races, genders, non-conforming identities, and even Boomers.

> @curbengh and I have "kindly" pointed out that he shouldn't bring up his personal political views to Hexo's contribution. @tcrowe then published the discussion in his "fight to communist" repo and claims me "left-leaning politically": <https://gitlab.com/tcrowe/code-communists/blob/master/projects/hexo/index.md>

> After his continuing bringing his personal political point of view into open-source multicultural community and promoting his conspiracy theory (sorts of "Communists" things), and considering me as his political enemy (by creating <https://gitlab.com/tcrowe/code-communists/blob/master/projects/hexo/index.md> page), we should start considering is it appropriate to have this "Persecution of dissidents" environment in Hexo team.

> Personally I am not worrying about any persecution from @tcrowe , or his describing me as "left-leaning") person. It is not about @tcrowe nor my personal political point of view, but @tcrowe 's behavior of hatred. What I really concern is his continuing hatred & misbehavior will eventually hurt the entire Hexo community.

> As a developer from China, I would like to use a Chinese idiom to describe my opinion: "池小容不得大鱼" (Small pond can not hold any big fish). We Hexo team is suitable for any voluntary contributors but not the "fighters" who has hatred to some other contributors from the entire multicultural open source community. @curbengh has said "We shouldn't care who is behind an Issue/PR (I don't even care if it's a dog/bot/martian), as long it's related to Hexo.". Personally I believe we should always keep that way. And that is exactly why we bring up our CoC, to avoid hatred and persecution based on personal political view (just like the hatred @tcrowe did to me).

> BTW, the CoC will be bringed up in hexojs/hexo#3962. @tomap, @curbengh, @segayuu and I have already approved that PR. if you have any idea about the CoC itself, you could go to <https://github.com/orgs/hexojs/teams/core/discussions/23> and share your idea.

### Commentary from tcrowe about alleged misbehavior

> @tcrowe has tried to bring his political idea about the Code of Conduct, saying the Code of Conduct is a tool used to persecute dissidents.

C.o.C. is a Liberal political tool that SukkaW and every other Liberal advocates. I'm informing them about it. They use it to tattle on people they disagree with.

> From the repo he posted, I found his showing hatred to the contributors from Node.js community:

They're lying to the group to say I am "showing hatred."  Notice SukkaW did not give them any specific quote. If there is hate it would be easy to show with copy+paste or a screen shot.

> @curbengh and I have "kindly" pointed out that he shouldn't bring up his personal political views to Hexo's contribution.

> After his continuing bringing his personal political point of view into open-source multicultural community

"Kindly" ?? 😂I'm not seeing the kindness. C.o.C. is their personal Liberal political view inside Hexo org that they initiated. Does "Multicultural" include people from other cultures? Like people who are not Liberals? Is that very diverse that none are representing other points of view?

> promoting his conspiracy theory (sorts of "Communists" things)

> considering me as his political enemy

> @tcrowe 's behavior of hatred.

> his describing me as "left-leaning") person.

This person is lying and whining about me to the whole organization and then playing the victim that I have considered them a political enemy. Without judging and without specific knowledge but I'm guessing that SukkaW is a woman or a gay man. They have a very feminine and false way of communicating. It lacks substance and there is also the language difficulty.

> What I really concern is his continuing hatred & misbehavior will eventually hurt the entire Hexo community.

> We Hexo team is suitable for any voluntary contributors but not the "fighters" who has hatred to some other contributors

If we say "hate" ten times maybe people will believe it.

> As a developer from China, I would like to use a Chinese idiom to describe my opinion: "池小容不得大鱼" (Small pond can not hold any big fish).

I think they're trying to accuse me of being arrogant. If you're a Mandarin speaker you can tell me about this saying.

In my culture we have a saying "Throwing mud and hoping it sticks." It's about people who lie repeatedly and hope you are fooled.

> And that is exactly why we bring up our CoC, to avoid hatred and persecution based on personal political view (just like the hatred @tcrowe did to me).

So after accusing me of "misbehavior," "hate," lying, and insulting me through the message that's why Hexo needs a C.o.C. To... protect... people... from abuse...

## Hexo's Multicultural Community

Hexo is a multi-lingual team and despite our differences we have been able to work together very well. Many of the contributors are not native English speakers but these C.o.C. documents are written by highly educated Liberals from American and European universities. A lot of knowledge of the language would be required to understand it. Do you think that they really understand the concepts inside the C.o.C. document? Or care?

Are they expecting us to believe that the Han Chinese majority in Hexo actually care about these Liberal policies from America? Chinese Communism is not a pro-homosexual, Liberal, ideology. They told us right from the beginning that it's to comply and conform to Netlify. That is the true attitude and thinking of many. To simply go along with the herd.

It's unlikely that anyone will do anything about SukkaW's lying. We should expect that they simply side with SukkaW because they're Chinese or ignore it because it's annoying them.

The whole C.o.C. discussion is a farce. There is only one culture there. But what do you call that culture?

## Compliance to San Francisco Netlify

+ <https://www.netlify.com/about/>
+ [Our article on Netlify](../netlify)

> We’re ~40% woman or non-binary, and have about half as many nationalities as we are team members!

They are telling you about a specific set of politics they have here. This is, by far, their most important statement about who they are. Their culture handbook has nothing about the real-life decisions. Those decisions boil down to if someone conforms to Liberal culture or not.

They're bragging that they are approximately half women and homosexuals. And the men don't matter. Did you think I was shocked when I read that?

> our HQ in San Francisco

San Francisco, California, home of Big Tech Censorship, home of many people that self-identify as a Communist or a Socialist. People that might... say... force you to have a Code of Conduct with Liberal values in it.

## Minimalization from ertrzyiks

I'm mapping out how people react so lets unpack this post from contributor [ertrzyiks](https://github.com/ertrzyiks).

> @tcrowe we are all hobbyists as far as I'm concerned, but even in professional remote conditions, you can't really expect people to react to your message within hours. It's exaggerated to say people did nothing in a thread that is alive for 2 days 🤷‍♂

They're talking about adding a C.o.C. which is an important document for some of the users. So much so that [at least three users are discussing](https://github.com/hexojs/hexo/pull/3962) it, what email address to report abuse to, and they want to comply with Netlify. I thought it was important enough to warn against it.

But you see, it's so unimportant to ertrzyiks that he needs to chime in only to tell you that.

> btw I skimmed over the CoC content and I'm ok with it.

He's fine with it. Forget what's in it like "empathy." He doesn't have to actually live by the C.o.C. and consider the others who do think this document is important. He's fine. SukkaW's lying and tcrowe would prefer you didn't let someone slander him. But ertrzyiks is fine with it.

The term for this type of thing is "minimalization" and being "dissmissive."

Update 2019-12-19: I didn't save the conversation but after I posted this page ertrzyiks insinuates I deserved the abuse because I called him a Communist. 😂 I didn't do that but it was funny and stupid. He regrets getting involved. I CAN IMAGINE.
