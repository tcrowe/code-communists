# Google

A corporate Communist organization.

<https://google.com>

## Table of Contents

+ [Machine Learning Fairness](#machine-learning-fairness)
+ [James Damore Outs Google as Ideological Echochamber](#james-damore-outs-google-as-ideological-echochamber)
+ [Project Veritas, Zach Voorhees, Google Machine Learning Fairness](#project-veritas-zach-voorhees-google-machine-learning-fairness)

## Machine Learning Fairness

a.k.a. ML Fairness
a.k.a. Algorthimic discrimination

Machine Learning Fairness is a method Google uses to rank down content by White men and boost up content created by non-white and non-male people. So, even if a piece of content by a White person is popular, and the most helpful, they will intentionally give you some irrelevant piece of information.

[More on the front page](../../#machine-learning-fairness) since this was one that started the Code Communists site.

## James Damore Outs Google as Ideological Echochamber

James Damore worked for Google and he blew open a story about Google's discrimination against straight white men and people with conservative politics.

[More on the front page](../../#james-damore-outs-google-as-ideological-echochamber) since this was one that started the Code Communists site.

## Project Veritas, Zach Voorhees, Google Machine Learning Fairness

[More on the front page](../../#project-veritas-zach-voorhees-google-machine-learning-fairness) since this was one that started the Code Communists site.
