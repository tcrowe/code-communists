# PayPal

A payment service.

<https://paypal.com>

Unpersoning is when you censor all the way to banking. You're not allowed to feed your family if PayPal disagrees with you.

## Table of Contents

+ [Unpersoning of Alex Jones](#unpersoning-of-alex-jones)
+ [Unpersoning of Jeranism](#unpersoning-of-jeranism)
+ [Unpersoning of Stefan Molyneux](#unpersoning-of-stefan-molyneux)

## Unpersoning of Alex Jones

Alex Jones and InfoWars publish media related to geopolitics, news, entertainment, and events. You're not allowed to bank with PayPal if you are Alex Jones and you talk about things they don't like.

PayPal Cuts Off Alex Jones’s Infowars, Joining Other Tech Giants <https://www.nytimes.com/2018/09/21/technology/paypal-blocks-infowars.html>

Alex Jones Files Lawsuit Trying to Force PayPal to Reinstate Infowars' Account <https://gizmodo.com/alex-jones-files-lawsuit-trying-to-force-paypal-to-rein-1829455496>

BOMBSHELL: PAYPAL BANS INFOWARS AFTER LOBBYING BY SOROS-FUNDED GROUP <https://www.infowars.com/bombshell-paypal-bans-infowars-after-lobbying-by-soros-funded-group/>

> Payment processor terminates agreement, citing “hate” and “intolerance”

## Unpersoning of Jeranism

Jeran is not allowed to bank with PayPal if he talks about Flat Earth.

JERANISM SUNDAY LIVE – PAYPAL BANNED ME! – WHAT’S NEXT? 8/4/19 <https://jeranism.com/jeranism-sunday-live-paypal-banned-me-whats-next-8-4-19/>

<https://www.youtube.com/watch?v=7IWF_1NXo20>

MOVING FORWARD FROM THE PAYPAL BAN <https://tfrlive.com/moving-forward-from-the-paypal-ban-87078/>

RAW #170 - 8/5/19 - PayPal Bannings & More! <https://www.youtube.com/watch?v=Rk9wXgV9Y8U>

## Unpersoning of Stefan Molyneux

You're not allowed to bank with PayPal if you talk about things in a philosophical way like Stefan Molyneux.

PayPal To Cut Off Donations to Right-Wing YouTuber Stefan Molyneux <https://www.newsweek.com/alt-right-youtuber-cut-off-paypal-donations-1469936>

PayPal Moves to Terminate Far-Right YouTube Personality Stefan Molyneux’s Account <https://www.rightwingwatch.org/post/paypal-moves-to-terminate-far-right-youtube-personality-stefan-molyneuxs-account/>

If you are Newsweek or a Jewish organization like Right Wing Watch you can label someone "Far-Right" and "Right-Wing" and complain to Big Tech to unperson.
