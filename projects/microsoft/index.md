# Microsoft

Software giant

<https://microsoft.com>

## Table of Contents

+ [Labor complaints](#labor-complaints)
+ [ML Fairness](#ml-fairness)
+ [Rampant allegations of misogyny](#rampant-allegations-of-misogyny)
+ [Immigration activism](#immigration-activism)
+ [Microsoft denounces their customer ICE](#microsoft-denounces-their-customer-ice)

## Labor complaints

<https://www.nlrb.gov/search/all/microsoft>

## ML Fairness

From the main page on [Machine Learning Fairness](../../readme.md#machine-learning-fairness)

## Rampant allegations of misogyny

Code Communists like to loudly proclaim to be pro-female. Does that bear out in reality?

<https://www.geekwire.com/2019/leaked-emails-women-microsoft-share-stories-alleged-sexual-harassment/>

<https://www.geekwire.com/2018/female-employees-microsoft-filed-238-discrimination-harassment-claims-7-year-period/>

<https://www.reuters.com/article/us-microsoft-women/microsoft-women-filed-238-discrimination-and-harassment-complaints-idUSKCN1GP077>

Class action lawsuit attempt and court denies Class Certification.

<https://microsoftgendercase.com/>

Court documents, rationale, and evidence

+ [./00_Class_Cert_Reply_Brief_Redacted_2-13.pdf](./00_Class_Cert_Reply_Brief_Redacted_2-13.pdf)
+ [./359-10_Farber_Rebuttal_MSFT_Redactions.pdf]\(./359-1./+ 0_Farber_Rebuttal_MSFT_Redactions.pdf)
+ [./359-11_Ryan_Reply_Report.pdf](./359-11_Ryan_Reply_Report.pdf)
+ [./359-12_Goldberg_MSFT_Redactions.pdf](./359-12_Goldberg_MSFT_Redactions.pdf)
+ [./File-Stamped-Complaint.pdf](./File-Stamped-Complaint.pdf)
+ [./Microsoft_Dkt_279-3_Complaint_Logs.pdf](./Microsoft_Dkt_279-3_Complaint_Logs.pdf)
  [Microsoft_Dkt_379_Ryan_Rep.pdf](./Microsoft_Dkt_379_Ryan_Rep.pdf)
+ [./Microsoft_Dkt_381_Motion_for_Class_Cert.pdf](./Microsoft_Dkt_381_Motion_for_Class_Cert.pdf)
+ [./Microsoft_Dkt_384_Expert_Report_of_Henry_Farber.pdf](./Microsoft_Dkt_384_Expert_Report_of_Henry_Farber.pdf)
+ [./Microsoft_Dkt_506_Order_Denying_Class_Certification.pdf]\(./M./+ icrosoft_Dkt_506_Order_Denying_Class_Certification.pdf)
+ [./Microsoft_Petition_for_Appeal.pdf](./Microsoft_Petition_for_Appeal.pdf)
  [microsoft_amended_complaint.pdf](./microsoft_amended_complaint.pdf)
+ [./plaintiffs-appellants-opening-brief-redacted.pdf](./plaintiffs-appellants-opening-brief-redacted.pdf)

Does [ML Fairness](../../readme.md#machine-learning-fairness) help them filter in more women to be come victims in the company? Is that how you can get more "sit on my lap" pay less for workers in a big Communist tech company?

## Immigration activism

<https://github.blog/2019-10-09-github-and-us-government-developers/>

> Microsoft has a long history of advocating for migrants and immigration law reform. Microsoft CEO Satya Nadella has spoken passionately about his own experience as an immigrant to the United States, and how Microsoft has consistently stood up for immigration policies that preserve every person’s dignity and human rights, and advocated for change.

<https://blogs.microsoft.com/on-the-issues/2018/06/19/the-country-needs-to-get-immigration-right/>

<https://www.linkedin.com/pulse/my-views-us-immigration-policy-satya-nadella/?published=t&trk=aff_src.aff-lilpar_c.partners_learning&irgwc=1>

> Our current cloud engagement with U.S. Immigration and Customs Enforcement (ICE) is supporting legacy mail, calendar, messaging and document management workloads.

> America is a nation of immigrants, and we’re able to attract people from around the world

The Founding Fathers had a [specific type of immigrant](https://infogalactic.com/info/Naturalization_Act_of_1790) they wanted. Microsoft could hire them to work remotely where it would bring wealth to a poor neighborhood. Instead they only take the smartest workers from those countries and bring them to U.S.

## Microsoft denounces their customer ICE

<https://www.linkedin.com/pulse/my-views-us-immigration-policy-satya-nadella/?published=t&trk=aff_src.aff-lilpar_c.partners_learning&irgwc=1>

> This new policy implemented on the border is simply cruel and abusive, and we are standing for change.

However, they will keep working together.
