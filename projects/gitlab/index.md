# gitlab

Git hosting company

<https://gitlab.com/>

Employees and customers of GitLab get to pay for:

## Table of Contents

+ [Diversity and Inclusion](#diversity-and-inclusion)
+ [CEO Sid Sijbrandij](#ceo-sid-sijbrandij)
+ [Diversity and Inclusion Partners](#diversity-and-inclusion-partners)
  + [Rails Girls](#rails-girls)
  + [AlterConf](#alterconf)
+ [Leadership](#leadership)
+ [Religion and politics at work](#religion-and-politics-at-work)
+ [Labor complaints](#labor-complaints)
+ [Telemetry](#telemetry)

## Diversity and Inclusion

<https://about.gitlab.com/handbook/values/#diversity--inclusion>

> We work to make everyone feel welcome and to increase the participation of underrepresented minorities and nationalities

> For example our sponsorship of diversity events and a double referral bonus.

> $2,000 referral bonus for hires from an "underrepresented group"

What is "underrepresented group" to GitLab?

> Women in Engineering - Globally
> Women in Leadership - Globally
> African American/Black - Globally
> Hispanic American/Latino - Globally
> Native American/Native - Globally
> Hawaiian/Pacific Islander - Globally
> Military Veteran - North America

What is the reason to seek out people based on their gender or race and boost up their "Representation" in GitLab? What is being Represented by doing that?

Isn't it illegal to discriminate based on race or gender or is incentives the loophole? Would that make you feel good as a man in that company that they prefer not to incentivize you to be there?

One of the core values is "Kindness."

> We value caring for others.

Is it caring to your men to give racial and gender preference to non-males and non-white people? Is it possible this hiring practice is designed to demoralize and exclude a certain group and give preferential treatment for anyone but that group?

Is it possible that this policy was a condition for the big funding? Would like to see how this got in there. 🤔

"GitLab tries to discriminate hiring process based on nationality to get US government contract" <https://www.reddit.com/r/programming/comments/drfhhg/gitlab_tries_to_discriminate_hiring_process_based/>

Ooooh, so that does happen. But in this case they were required to shut out Chinese, Russians, and Ukrainians from certain positions. But aren't the company founders Ukrainian? Yes. It turns out that they protested in the threads.

+ <https://gitlab.com/gitlab-com/www-gitlab-com/issues/5554>
+ <https://gitlab.com/gitlab-com/www-gitlab-com/issues/5555>
+ <https://about.gitlab.com/blog/2019/11/12/update-on-hiring/>

This issue seems to have rustled a lot of jimmies in the process. Internal and external protestors weighed in with their disgust and talking about the limits of GitLab "Diversity and Inclusion." Was that the point of the contracts from the Enterprise customers?

User [mengxin's protest](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/24888)

![./mengxin-protest.png](./mengxin-protest.png)

User [kysglab's protest](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34105)

![./kysglab-protest01.png](./kysglab-protest01.png)
![./kysglab-protest02.png](./kysglab-protest02.png)

Some more [internal dissenters](https://gitlab.com/jburrows001) and a few [trolls](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34037) added in their contributions.

The drama was enough to [attract attention from WSJ](https://www.wsj.com/articles/resignation-at-gitlab-highlights-concerns-over-corporate-espionage-11573686134)

> Candice Ciresi resigned from her position as director of global risk and compliance at the San Francisco-based software-development startup following a brouhaha that began last month over how the company handled client concerns about data privacy.

> The fast-growing firm, which counts several big banks and government agencies as its clients, said in September it had raised $268 million.

It was only for two roles, not all employees, but it was enough to cause a reaction. Did any of these contracts include clauses for other "diversity" programs? It would be interesting if someone had a tool to look at the company pages over time and compare with the new Enterprise customers.

<https://about.gitlab.com/handbook/anti-harassment>

> Discrimination Any form of discrimination towards an individual is strictly prohibited. Types of discrimination include:
>   Age
>   Disability
>   Race; including color, nationality, ethnicity, or national origin
>   Religious belief or lack of religious belief
>   Life expectancy
>   Sex
>   Sexual orientation
>   Transgender status

Interviewing and selecting candidates is another term for discrimination. If you were not discriminating then you would end up with randomly selected employees which don't have the correct skill. Incentivizing one kind of quality or behavior is another type of discrimination.

> puprposely misgendering people, such as consistently referring to someone as 'he' after repeated requests to use 'she' or vice versa.

[O'Brien: How many fingers am I holding up?](https://www.youtube.com/watch?v=wTFV9w4B0eg)

> The company will not tolerate fighting, bullying, coercion

Unless you're coercing people to conform to Liberal ideology like gender inventions.

## CEO Sid Sijbrandij

<https://twitter.com/sytses>

![./sid-sijbrandij-twitter.png](./sid-sijbrandij-twitter.png)

If you were a masculine male would you get a fair interview with a person that prioritizes signaling their conformity to gender pronouns? `he/him`

[September 23, 2019: "We do not discuss politics in the workplace"](https://gitlab.com/gitlab-com/www-gitlab-com/commit/3b28b6a0ed25125a30d774bc8180e0e1bdf40f77#82fcc091d7b41a990f8e226f179677504e294814_233_238)

CEO Sijbrandij is in a difficult position because many of the employees, being hired based on race and gender, are probably not keen to obey this rule.

## Diversity and Inclusion Partners

<https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/job-families/people-ops/diversity-inclusion-partner/index.html.md>

> Works with the Recruiting team to create and implement a diversity recruitment strategy

> Performance Indicators
> Women globally as a whole at GitLab
> Women in leadership
> Women voluntary attrition
> Pay equality
> Engagement survey inclusion score

Diversity and Inclusion = Select based on gender?

### Rails Girls

+ <https://about.gitlab.com/blog/2016/03/24/sponsorship-update/>
+ <https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/blog/posts/2016-02-02-gitlab-diversity-sponsorship.html.md>

Funding groups based on their gender.

### AlterConf

<https://www.alterconf.com/>

> Thanks For Helping Us Change The Face Of Tech & Gaming!

The mostly white male team and customers at GitLab are paying to

> safe opportunities for marginalized people

## Leadership

Huge team page <https://about.gitlab.com/company/team/>

Top 30 lightly investigated and most seem like smart tech guys.

## Religion and politics at work

<https://about.gitlab.com/handbook/values/#religion-and-politics-at-work->

> we encourage free discussion of operational decisions that can move us toward being a more inclusive company. GitLab also publicly supports pro-diversity activities and events.

"We do not discuss politics in the workplace" unless it's Liberal ideology.

## Labor complaints

Zero are registered in The State of California at the time of writing this. <https://www.nlrb.gov/search/all/gitlab>

## Telemetry

*possibly unrelated but curious*

October 10, 2019 GitLab announced a new telemetry feature and then backpedaled. Did this have anything to do with the new contracts from Enterprise customers?

<https://www.theregister.co.uk/2019/10/30/gitlab_backtracks_on_plan_to_add_usage_telemetry_after_user_protests/>

<https://gitlab.com/gitlab-com/www-gitlab-com/issues/5672#note_235182171>
