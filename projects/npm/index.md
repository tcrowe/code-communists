# npm

<https://npmjs.com>

## Table of Contents

+ [Labor complaints](#labor-complaints)
+ [NPM CEO Isaac Schlueter calls for fewer white men in tech](#npm-ceo-isaac-schlueter-calls-for-fewer-white-men-in-tech)
+ [NPM Hell for Employees](#npm-hell-for-employees)
+ [Module Unpublish Protests](#module-unpublish-protests)

## Labor complaints

<https://www.nlrb.gov/search/all/npm>

## NPM CEO Isaac Schlueter calls for fewer white men in tech

<https://dailycaller.com/2017/09/25/tech-ceo-isaac-schlueter-calls-for-fewer-white-men-in-tech/>

## NPM Hell for Employees

When Commie employees unionize against your Commie company

+ <https://www.theregister.co.uk/2019/04/01/npm_layoff_staff/>
+ <https://www.theregister.co.uk/2019/04/22/npm_fired_staff_union_complaints/>

## Module Unpublish Protests

<https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/>
