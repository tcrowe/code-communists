Donate NAV for my OSINT report `NTYZzTA3QZ25eG8cpVK8JqYcL631HqPXVR`

# NavCoin

NavCoin cryptocurrency. If you review the sentiment of the leadership could you say it's fair or unbiased?

<https://navcoin.org/>
<https://navhub.org/>

## Table of Contents

+ [Discuss](#discuss)
+ [Community fund](#community-fund)
+ [Social media](#social-media)
+ [Nav4Navajo racial campaign](#nav4navajo-racial-campaign)
+ [Responses from NavCoin leaders](#responses-from-navcoin-leaders)

## Discuss

<https://www.reddit.com/user/tc-hodl-and-buidl/comments/el0qux/code_communists_navcoin_cryptocurrency/>

<https://twitter.com/tcbuidl/status/1214298332090314753>

## Community fund

Not everyone agrees with the Liberal sentimental fund policies. The moderator Goku explicitly denied sympathy toward Communism and Socialism. He gave a resource to check for decisions regarding funds.

+ [Accepted proposals](https://www.navexplorer.com/community-fund/proposals/accepted)
+ [Rejected proposals](https://www.navexplorer.com/community-fund/proposals/rejected)

The politically charged event in New Zealand caused some response and discussion from the community.

<https://www.reddit.com/r/NavCoin/comments/b1cqj3/community_fund_proposal_donation_for_the_victims/>

## Social media

![./navcoin-anti-white.png](./navcoin-anti-white.png)
![./prole-communist.png](./prole-communist.png)
[./craig-mcgregor-anti-white01.png](./craig-mcgregor-anti-white01.png)
![./craig-mcgregor-anti-white02.png](./craig-mcgregor-anti-white02.png)
![./craig-mcgregor-carbon-tax01.png](./craig-mcgregor-carbon-tax01.png)
![./craig-mcgregor-carbon-tax02.png](./craig-mcgregor-carbon-tax02.png)
![./craig-mcgregor-carbon-tax03.png](./craig-mcgregor-carbon-tax03.png)
![./craig-mcgregor-greta-thunberg.png](./craig-mcgregor-greta-thunberg.png)
![./craig-mcgregor-profile.png](./craig-mcgregor-profile.png)
![./prole-anti-trump.png](./prole-anti-trump.png)
![./navcoin-carbon-tax.png](./navcoin-carbon-tax.png)
![./beezy-profile.png](./beezy-profile.png)
![./colvano-navajo-advocate.png](./colvano-navajo-advocate.png)
![./jonathan-anti-trump01.png](./jonathan-anti-trump01.png)
![./jonathan-anti-trump02.png](./jonathan-anti-trump02.png)
![./jonathan-beezy-trump.png](./jonathan-beezy-trump.png)
![./jonathan-censor01.png](./jonathan-censor01.png)
![./jonathan-censor02.png](./jonathan-censor02.png)
![./jonathan-censor03.png](./jonathan-censor03.png)
![./jonathan-censor04.png](./jonathan-censor04.png)
![./jonathan-discord.png](./jonathan-discord.png)
![./juguelio-anti-trump-censor.png](./juguelio-anti-trump-censor.png)
![./juguelio-anti-trump01.png](./juguelio-anti-trump01.png)
![./juguelio-anti-white01.png](./juguelio-anti-white01.png)
![./juguelio-anti-white02.png](./juguelio-anti-white02.png)
![./juguelio-feminist.png](./juguelio-feminist.png)
![./juguelio-reddit01.png](./juguelio-reddit01.png)
![./mntyfrsh-anti-trump01.png](./mntyfrsh-anti-trump01.png)
![./mntyfrsh-anti-trump02.png](./mntyfrsh-anti-trump02.png)
![./mntyfrsh-anti-trump03.png](./mntyfrsh-anti-trump03.png)
![./mntyfrsh-anti-trump04.png](./mntyfrsh-anti-trump04.png)
![./mntyfrsh-anti-trump05.png](./mntyfrsh-anti-trump05.png)
![./mntyfrsh-anti-trump06.png](./mntyfrsh-anti-trump06.png)
![./mntyfrsh-anti-trump07.png](./mntyfrsh-anti-trump07.png)
![./mntyfrsh-anti-trump08.png](./mntyfrsh-anti-trump08.png)
![./mntyfrsh-anti-white01.png](./mntyfrsh-anti-white01.png)
![./mntyfrsh-bizarre-elistist-rant.png](./mntyfrsh-bizarre-elistist-rant.png)
![./mntyfrsh-communist.png](./mntyfrsh-communist.png)
![./mntyfrsh-discord.png](./mntyfrsh-discord.png)
![./rogier-van-der-beek-carbon-tax.png](./rogier-van-der-beek-carbon-tax.png)
![./salmonskinrole-discord.png](./salmonskinrole-discord.png)
![./spiritar3-discord.png](./spiritar3-discord.png)
![./spiritar3-navajo-advocate01.png](./spiritar3-navajo-advocate01.png)
![./spiritar3-navajo-advocate02.png](./spiritar3-navajo-advocate02.png)
![./spiritar3-navajo-advocate03.png](./spiritar3-navajo-advocate03.png)

## Nav4Navajo racial campaign

+ [nav4navajo-campaign.pdf](./nav4navajo-campaign.pdf)
+ [nav4navajo-campaign.txt](./nav4navajo-campaign.txt)
+ [nav4navajo-campaign.zip](./nav4navajo-campaign.zip)

Living here in Utah, I've met some Navajo tribesmen so this was interesting. Feel free to add more about Bears Ears and Navajo here. -TC

## Responses from NavCoin leaders

You have to go through the usual *jeering, sneering, and smearing* with Communists. If they could throw poop, a milkshake, or acid at you they would but it's online so you're safe.

![./prodpeak-response01.png](./prodpeak-response01.png)

Done!

![./juguelio-response05.png](./juguelio-response05.png)
![./juguelio-response03.png](./juguelio-response03.png)
![./juguelio-response04.png](./juguelio-response04.png)

The idea is to throw poop and joke in hopes that nobody takes it seriously. It's like the Tar and Feather of olde. That person Juguelio is particularly foul if you look at his posts above. Lets get past that and into some substance.

![./juguelio-lordnelsson-response.png](./juguelio-lordnelsson-response.png)

Carbon tax is part of Communist population reduction. When you lower energy use and tax people have less children and some die from the cold or heat.

![./prole-response01.png](./prole-response01.png)
![./prole-response02.png](./prole-response02.png)
![./goku-response01.png](./goku-response01.png)
![./goku-response02.png](./goku-response02.png)
![./goku-response03.png](./goku-response03.png)

> does not necessarily represent NavCoin as a collective.

He's a smart guy so he put "necessarily" and he is right. It doesn't necessarily represent them but there was only one mod, Goku, who said he was not a Leftist and helped with resources and counter-examples. Eventually he'll be given the silent treatment, his ideas rejected, and they'll do their usual naysaying at him until he leaves. Dissenters are not typically not tolerated by Communists.

See [The views of some don't represent all](../../readme.md#the-views-of-some-dont-represent-all). In short, one dissenter does not disprove a generalization.

![./lordnelsson-response01.png](./lordnelsson-response01.png)
![./rcavalli-response01.png](rcavalli-response01.png)
![./rcavalli-response02.png](rcavalli-response02.png)

See [That's your opinion](../../readme.md#thats-your-opinion). Postmodernism is a part of their ideology so when evidence is on their side its true and when it's on your side it's an opinion.

![./aguycalled-response01.png](./aguycalled-response01.png)
![./evidence-opinions-response.png](./evidence-opinions-response.png)
![./juguelio-response01.png](./juguelio-response01.png)
![./juguelio-response02.png](./juguelio-response02.png)
![./lordnelsson-response02.png](./lordnelsson-response02.png)
![./lordnelsson-response03.png](./lordnelsson-response03.png)
![./lordnelsson-response04.png](./lordnelsson-response04.png)
![./lordnelsson-response05.png](./lordnelsson-response05.png)

+ See [Liberal is not Communist](../../readme.md#liberal-is-not-communist)
+ See [Socialist is not Communist](../../readme.md#socialist-is-not-communist)
+ See [Progressive is not Communist](../../readme.md#progressive-is-not-communist)
+ See [I'm not a Communist](../../readme.md#im-not-a-communist)
