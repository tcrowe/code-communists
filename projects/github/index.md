# GitHub

<https://github.com>

> We also joined an amicus brief last year to protect Sanctuary Cities.

## Table of Contents

+ [GitHub publicly denounces ICE as a customer](#github-publicly-denounces-ice-as-a-customer)

## GitHub publicly denounces ICE as a customer

<https://github.blog/2019-10-09-github-and-us-government-developers/>

> Like many Hubbers, I strongly disagree with many of the current administration’s immigration policies, including the practice of separating families at the border, the Muslim travel ban, and the efforts to dismantle the DACA program that protects people brought to the U.S. as children without documentation. The leadership team shares these views. These policies run counter to our values as a company, and to our ethics as people. We have spoken out as a company against these practices, and joined with other companies in protesting them. You can read our public statements in the Keep the Families Together Act letter, the Muslim travel ban amicus briefs, and the DACA business leaders letter of support. We also joined an amicus brief last year to protect Sanctuary Cities.

> We will donate $500,000—in excess of the value of the purchase by ICE—to nonprofit organizations working to support immigrant communities targeted by the current administration. The Social Impact team will get Hubber input on which nonprofits can provide the most impact for migrants, to be supported via this donation. In the past, we’ve supported organizations like Border Angels, Families for Freedom, and Movement on the Ground.

Wow! It's quite ballsy!
