# reddit

Social media site

>  San Francisco, CA

Shocker!

> Bias towards action

Does that include political action?

+ <https://github.com/reddit>
+ <https://www.reddit.com/r/redditdev/>
+ <https://www.redditinc.com/careers/>

## Table of Contents

+ [Autoflagging censorship](#autoflagging-censorship)
+ [The Donald](#the-donald)
+ [Europeans](#europeans)
+ [r/SaltLakeCity](#rsaltlakecity)
+ [Mods](#mods)

## Autoflagging censorship

I posted on my own profile and it was flagged. If they don't like the content it's not allowed to be published. In this case I was trying to clear someone's name and give evidence of their tolerance. Now reddit users cannot see the new information.

![./reddit-autoflagging-censorship.png](reddit-autoflagging-censorship.png)

What are the internal mechanisms in code that flag these? Human, bot, or both? Is it [ML Fairness](../../readme.md#machine-learning-fairness)?

## The Donald

They don't like The Donald. Communists wish to overthrow the Democratically elected President.

<https://www.breitbart.com/tech/2019/06/26/reddit-censors-the_donald-community-of-750000-trump-supporters/>

<https://newrightnetwork.com/2019/06/reddit-quarantines-donald-censorship.html/>

<https://reclaimthenet.org/the-donald-win-reddit-new-forum-website/>

<https://summit.news/2019/06/28/dem-senator-pushed-for-censorship-of-the-donald-before-it-was-quarantined-by-reddit/>

Word on the street is that they were uncovering too many plots and crimes on there so it had to go down. I bet it's expensive to monitor and do all that thought policing too. [ML Fairness](../../readme.md#machine-learning-fairness), for sure, will get costs down but there is also loss of trust, coders to maintain, and damage to the brand from totalitarian Communist censorship.

More on [Mods](#mods)

## Europeans

They don't like Europeans.

<https://www.foxnews.com/tech/reddit-administrators-accused-of-censorship>

## r/SaltLakeCity

tcrowe: I was instantly pwned by Leftists after I posted in r/SaltLakeCity about the need to close the border. There were a handful of cussing hostile nutters. Another handful were naysayers. Maybe the mods nuked the post eventually or the downvotes killed it. More information is needed to understand the whole method.

How do they get these mods on there?

What happens if you create something like r/AltSaltLakeCity? (Will try later)

## Mods

Mod leaks

<https://theralphretort.com/more-from-the-reddit-mod-leaks-3021015/>

<https://twitter.com/xaviermendel>

<https://pastebin.com/HLhhyKbG>

[./#modtalkleaks_part_3.txt](./#modtalkleaks_part_3.txt)
