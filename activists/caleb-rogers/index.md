# Caleb Rogers

+ <https://github.com/komali2>
+ <https://www.calebjay.com/>

From Roger's page about values:

> All people are equal. Race, gender, and dynastic privilege exist currently and that is something that we should equalize. However, there is no scientific basis for race, and racists are stupid as hell.

Are you looking forward to your equalization with people like Rogers in control?

> Information should be free and easily accessed. In fact, it should be actively given to people that could find it useful. So therefore, efforts to hide, obscure, or obfuscate information are bad. This means I support libraries and the taxes that pay for them, the free software movement and the organizations that support it such as GNU, whistleblowers, journalists, grey-hat hackers, bio-hackers. This means I don't support efforts to suppress the free flow of information...

You'll see the irony of this statement later. This person has decided to go on the offensive against one of the authors of this repository, Tony Crowe.

## Table of Contents

+ [The meeting](#the-meeting)
+ [Rogers proclaims free speech and flags Crowe's content](#rogers-proclaims-free-speech-and-flags-crowes-content)
+ [Rogers contacts Crowe's employer](#rogers-contacts-crowes-employer)
+ [San Francisco filled up with feces, urine, and needles](#san-francisco-filled-up-with-feces-urine-and-needles)
+ [Social media](#social-media)
  + [Abortion](#abortion)
  + [Christianity](#christianity)
  + [Antisemitism](#antisemitism)
  + [Trump](#trump)
  + [Cops](#cops)
  + [White people](#white-people)
  + [Black people](#black-people)
  + [Communism](#communism)
  + [Drug addiction](#drug-addiction)
  + [Women](#women)
  + [LGBT](#lgbt)
  + [Terrorism](#terrorism)

## The meeting

[Rogers is talking down to a homeless person](https://github.com/botupdate/botupdate/issues/1) who is publishing information on GitHub.

> Seek help.

\-Rogers

Caleb Rogers, an LGBT activist, is cautioned against patronizing people for mental health if he is ignorant. He is cautioned against haughty words [while his city is covered in human waste.](https://www.youtube.com/results?search_query=san+francisco+covered+in+poop)

> @komali2 You should be more careful about your words considering you're living in a place like SF. It doesn't look good to patronize people while your Communist city is covered in human feces and urine. When you flee that city in shame and cowardice don't come to Salt Lake City. We prefer things clean and orderly.

Rogers responds to the warning from Crowe by giving mocking and illiterate reply.

Thread backup: [1](./botupdate-thread01.png) [2](./botupdate-thread02.png) [3](./botupdate-thread03.png) [4](./botupdate-thread04.png) [5](./botupdate-thread05.png)

## Rogers proclaims free speech and flags Crowe's content

Rogers was told by Crowe his city, San Francisco, was covered in human feces and he was cautioned against haughty words and insults to the homeless. Rogers, in response, began to send messages to associates of Tony Crowe about Roger's politics and Crowe's politics.

[GitHub responded by disabling two of Crowe's repositories](https://gitlab.com/tcrowe/github-communist-totalitarians) that contain a large amount of information about tech censorship and extreme far-left political action against their enemies. If Rogers flags content does that conflict with [his value from his 'about me' values section?](https://www.calebjay.com/about.html#values)

![./values-anti-censorship.png](./values-anti-censorship.png)

## Rogers contacts Crowe's employer

Rogers sent an email to the general contact at the company Crowe works for. He is making accusations that Tony Crowe "hates" this group or that group.

```
Von: Caleb Rogers < [ redacted ] @gmail.com>
Datum: 1/22/2020 7:36:56 PM
Betreff: Check out your engineer, Tony Crowe
An: kontakt@youreadfor.me
Hi there,

Thought you might be interested in the kind of things your engineer Tony Crowe says. Man does that guy hate gay people and non-whites!

https://github.com/botupdate/botupdate/issues/1#issuecomment-574868895

https://twitter.com/tcbuidl/status/1214308036900278273?s=20

https://tcrowe.commons.host/2019/12/21/leaving-hexo-project-and-rethinking-open-source-and-foss/

What a card!

Have a good day :)

Love,
Caleb
```

Edit 2021-08-07: [This page he references is moved.](+https://tonycrowe.com/2019/12/21/leaving-hexo-project-and-rethinking-open-source-and-foss/)

This strategy is common of the Left. They are accusing their political enemies of "hate." They do not offer any counter evidence or counter reasoning.

Within the society it's not only tolerated but encouraged at this time in history! Most companies having taken on some amount of Diversity Hires and they punish anyone who disagrees. While Rogers is illiterate and a fool the dominant force in society are protecting and encouraging him while discouraging facts of it.

The whole of society are in a state similar to Rogers where knowledge, reason, and evidence are irrelevant except to attack their enemy. If the evidence is against them they pretend it doesn't exist, attack it, or censor it. At one time a person can proclaim free speech and the next moment censorship.

## San Francisco filled up with feces, urine, and needles

If you weren't familiar to the references about San Francisco, under Liberal occupation of the city they have transformed it. The city is now covered in human feces and urine. They distribute needles so drug users can shoot heroin-like substances into their bloodstream.

YouTube search "San Francisco covered in poop" <https://www.youtube.com/results?search_query=san+francisco+covered+in+poop>

They are sometimes interviewing children who are walking past piles of trash and trying to avoid the poop and needles.

"Justice-Involved Person" the New Felon in San Francisco by Styxhexenhammer666 <https://www.bitchute.com/video/05abokfJ3Ns/>

Check out the new project San Francisco Covered in Poop:

+ <https://gitlab.com/tcrowe/san-francisco-covered-in-poop>
+ <https://gitlab.com/tcrowe/san-francisco-covered-in-poop>

a.k.a. De-criminalization

In Dystopian San Francisco, City to Pay Six Figure Salaries to Turd Removal Squads by Styxhexenhammer666 <https://www.bitchute.com/video/22mYbWn3APg/>

## Social media

![./respect.png](./respect.png)
![./bully.png](./bully.png)
![./harassment.png](./harassment.png)
![./guillotine01.png](./guillotine01.png)

![./toxic01.png](./toxic01.png)
![./toxic02.png](./toxic02.png)
![./toxic03.png](./toxic03.png)
![./toxic04.png](./toxic04.png)
![./toxic05.png](./toxic05.png)
![./toxic06.png](./toxic06.png)
![./toxic07.png](./toxic07.png)
![./toxic08.png](./toxic08.png)
![./toxic09.png](./toxic09.png)
![./toxic10.png](./toxic10.png)
![./toxic11.png](./toxic11.png)
![./toxic12.png](./toxic12.png)
![./toxic13.png](./toxic13.png)
![./toxic14.png](./toxic14.png)
![./toxic15.png](./toxic15.png)

### Abortion

![./abortion01.png](./abortion01.png)
![./abortion02.png](./abortion02.png)
![./abortion03.png](./abortion03.png)
![./abortion04.png](./abortion04.png)
![./abortion05.png](./abortion05.png)
![./abortion06.png](./abortion06.png)
![./abortion07.png](./abortion07.png)
![./abortion08.png](./abortion08.png)

### Christianity

![./anti-christian01.png](./anti-christian01.png)
![./anti-christian02.png](./anti-christian02.png)
![./anti-christian03.png](./anti-christian03.png)
![./anti-christian04.png](./anti-christian04.png)
![./anti-christian05.png](./anti-christian05.png)
![./anti-christian06.png](./anti-christian06.png)
![./anti-christian07.png](./anti-christian07.png)
![./anti-christian08.png](./anti-christian08.png)
![./anti-christian09.png](./anti-christian09.png)
![./anti-christian10.png](./anti-christian10.png)

### Antisemitism

![./anti-jewish01.png](./anti-jewish01.png)
![./anti-jewish02.png](./anti-jewish02.png)
![./anti-jewish03.png](./anti-jewish03.png)

### Trump

![./anti-trump01.png](./anti-trump01.png)
![./anti-trump02.png](./anti-trump02.png)
![./anti-trump03.png](./anti-trump03.png)

### Cops

![./anti-white-cop01.png](./anti-white-cop01.png)
![./anti-white-cop02.png](./anti-white-cop02.png)

### White people

![./anti-white01.png](./anti-white01.png)
![./anti-white02.png](./anti-white02.png)
![./anti-white03.png](./anti-white03.png)
![./anti-white04.png](./anti-white04.png)
![./anti-white05.png](./anti-white05.png)
![./anti-white06.png](./anti-white06.png)
![./anti-white07.png](./anti-white07.png)
![./anti-white08.png](./anti-white08.png)
![./anti-white09.png](./anti-white09.png)
![./anti-white10.png](./anti-white10.png)
![./anti-white11.png](./anti-white11.png)
![./anti-white12.png](./anti-white12.png)
![./anti-white13.png](./anti-white13.png)
![./anti-white14.png](./anti-white14.png)
![./anti-white15.png](./anti-white15.png)

### Black people

![./black-activist01.png](./black-activist01.png)
![./black-activist02.png](./black-activist02.png)
![./black-activist03.png](./black-activist03.png)
![./black-activist04.png](./black-activist04.png)
![./black-activist05.png](./black-activist05.png)
![./black-activist07.png](./black-activist07.png)
![./black-activist08.png](./black-activist08.png)
![./black-activist09.png](./black-activist09.png)
![./black-activist10.png](./black-activist10.png)

### Communism

![./communism01.png](./communism01.png)
![./communist01.png](./communist01.png)
![./communist02.png](./communist02.png)
![./communist03.png](./communist03.png)

### Drug addiction

![./drug-addict01.png](./drug-addict01.png)
![./drug-addict03.png](./drug-addict03.png)
![./drug-addict04.png](./drug-addict04.png)
![./drug-addict05.png](./drug-addict05.png)
![./drug-addict06.png](./drug-addict06.png)
![./drug-addict07.png](./drug-addict07.png)
![./drug-addict08.png](./drug-addict08.png)

### Women

![./feminist01.png](./feminist01.png)

### LGBT

![./lgbt-activist01.png](./lgbt-activist01.png)
![./lgbt-activist02.png](./lgbt-activist02.png)
![./lgbt-activist03.png](./lgbt-activist03.png)
![./lgbt-activist04.png](./lgbt-activist04.png)
![./lgbt-activist05.png](./lgbt-activist05.png)
![./lgbt-activist06.png](./lgbt-activist06.png)
![./lgbt-activist07.png](./lgbt-activist07.png)
![./lgbt-activist08.png](./lgbt-activist08.png)
![./lgbt-activist09.png](./lgbt-activist09.png)
![./lgbt-activist10.png](./lgbt-activist10.png)
![./lgbt-activist11.png](./lgbt-activist11.png)
![./lgbt-activist12.png](./lgbt-activist12.png)

### Terrorism

![./terrorist01.png](./terrorist01.png)
![./terrorist02.png](./terrorist02.png)
